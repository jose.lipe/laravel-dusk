<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

/**
 * @group pages_crud
 */
class PagesTest extends DuskTestCase
{
    use DatabaseMigrations;
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testHomeOfPages()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/pages')
                    ->assertSee('Páginas');
        });
    }

    public function testAddNewPage()
    {
        $this->browse(function(Browser $browser) {
            $browser->visit('pages/create')
                ->type('title', 'Teste de Titulo')
                ->type('body', 'Conteúdo de Página')
                ->press('salvar')
                ->assertPathIs('/pages')
                ->assertSee('Teste de Titulo');
        });
    }

    public function testRemovePage()
    {
        $page = factory(\App\Page::class)->create([
            'title' => 'Página de Teste'
        ]);

        $this->browse(function(Browser $browser) use ($page) {
            $browser->visit('pages/' . $page->id)
                ->press('remove')
                ->assertPathIs('/pages')
                ->assertDontSee('Página de Teste');
        });
    }

    /**
     * @group pages_crud_navigation
     */
    public function testNavigation()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/pages')
                ->press('#btnNovo')
                ->assertPathIs('/pages/create');
        });
    }
}
